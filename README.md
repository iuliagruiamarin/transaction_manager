This microservice put FinancialTransaction instances wrapped up in JMS Messages to the TRANSACTION.QUEUE.
A message listener is configured to process the message and send a validation message on the VALIDATION.QUEUE.
Another listener is defined that waits for the validate and persist the transaction into database. 

For demo purpose Transaction-manager starts the broker MQ and creates the queues. Please run it first! 

API endpoints:
1. Generate Transaction Report:
    Curl: curl -X GET "http://localhost:9090/transaction/report/Ion%20Ionescu" -H "accept: application/json"
    Request URL: http://localhost:9090/transaction/report/Ion%20Ionescu
    Swagger UI: http://localhost:9090/swagger-ui.html
2. Create Transaction: 
    Curl - Invalid Format: curl -X POST "http://localhost:9090/transaction" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 0, \"cnpPayee\": \"123\", \"cnpPayer\": \"string\", \"description\": \"string\", \"ibanPayee\": \"string\", \"ibanPayer\": \"string\", \"payeeName\": \"string\", \"payerName\": \"string\", \"transactionType\": \"IBAN_TO_IBAN\", \"walletPayee\": \"string\", \"walletPayer\": \"string\"}"
    Curl - Valid Format: curl -X POST "http://localhost:9090/transaction" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"amount\": 100, \"cnpPayee\": \"2931025125781\", \"cnpPayer\": \"2931025125781\", \"description\": \"random\", \"ibanPayee\": \"RO19RZBR8637876275195736\", \"ibanPayer\": \"RO19RZBR8637876275195736\", \"payeeName\": \"string\", \"payerName\": \"string\", \"transactionType\": \"IBAN_TO_IBAN\", \"walletPayee\": \"string\", \"walletPayer\": \"string\"}"
    Request URL: http://localhost:9090/transaction
    Swagger UI: http://localhost:9090/swagger-ui.html