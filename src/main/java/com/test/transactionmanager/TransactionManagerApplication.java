
package com.test.transactionmanager;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
@EnableJms
public class TransactionManagerApplication {

	@Value("${spring.activemq.broker-url}")
	private String brokerUrl;

	@Bean
    public BrokerService broker() throws Exception {
        BrokerService broker = new BrokerService();
        broker.addConnector(brokerUrl);
        return broker;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        return new JmsTemplate(cachingConnectionFactory());
    }

    @Bean
	public ActiveMQConnectionFactory senderActiveMQConnectionFactory(){
    	ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
    	activeMQConnectionFactory.setBrokerURL(brokerUrl);
    	return activeMQConnectionFactory;
	}

	@Bean
	public CachingConnectionFactory cachingConnectionFactory(){
		return new CachingConnectionFactory(senderActiveMQConnectionFactory());
	}

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(senderActiveMQConnectionFactory());
        return factory;
    }

    public static void main(String[] args) {
        SpringApplication.run(TransactionManagerApplication.class, args);
    }

}
