package com.test.transactionmanager.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.test.transactionmanager.dto.Report;
import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping(path = "/{payerName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<List<FinancialTransaction>> retrieveFinancialTransactionsByPayerName(@PathVariable String payerName) {
        return ResponseEntity.ok(transactionService.loadFinancialTransactionsByPayerName(payerName));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity createFinancialTransaction(@RequestBody FinancialTransaction financialTransaction) throws JsonProcessingException {
        return new ResponseEntity<>(transactionService.createFinancialTransaction(financialTransaction), HttpStatus.CREATED);
    }

    @GetMapping(path = "/report/{payerName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Report> generateFinancialTransactionsReportByPayerName(@PathVariable String payerName) {
        return ResponseEntity.ok(transactionService.generateFinancialTransactionsReportByPayerName(payerName));
    }
}
