package com.test.transactionmanager.exception;

import com.test.transactionmanager.dto.ValidationMessageDTO;
import com.test.transactionmanager.dto.ValidationStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TransactionManagerExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({TransactionValidationException.class})
    public ResponseEntity handleValidationError(TransactionValidationException ex) {
        return new ResponseEntity<>(new ValidationMessageDTO(ValidationStatus.INVALID, ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
