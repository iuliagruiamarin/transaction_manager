package com.test.transactionmanager.exception;

public class TransactionValidationException extends RuntimeException {
    public TransactionValidationException() {
    }

    public TransactionValidationException(String message) {
        super(message);
    }
}
