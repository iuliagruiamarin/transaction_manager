package com.test.transactionmanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.corba.se.spi.orbutil.threadpool.ThreadPool;
import com.test.transactionmanager.dao.TransactionRepository;
import com.test.transactionmanager.dto.Report;
import com.test.transactionmanager.dto.TransactionSummary;
import com.test.transactionmanager.dto.ValidationMessageDTO;
import com.test.transactionmanager.dto.ValidationStatus;
import com.test.transactionmanager.exception.TransactionValidationException;
import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.model.enums.TransactionType;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import sun.plugin.dom.exception.InvalidStateException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ActiveMQConnectionFactory connectionFactory;

    private Map<String, ValidationMessageDTO> validationMessages = new HashMap<>();

    private Map<String, FinancialTransaction> financialTransactions = new HashMap<>();

    private ObjectMapper mapper = new ObjectMapper();

    private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Override
    public FinancialTransaction createFinancialTransaction(FinancialTransaction financialTransaction) throws JsonProcessingException {
        financialTransaction.setId(String.valueOf(UUID.randomUUID().getMostSignificantBits()));
        jmsTemplate.send("TRANSACTION.QUEUE", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message message = null;
                try {
                    message = session.createTextMessage(mapper.writeValueAsString(financialTransaction));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                message.setStringProperty("ftid", financialTransaction.getId());
                message.setJMSReplyTo(session.createQueue("VALIDATION.QUEUE"));
                financialTransactions.put(message.getJMSMessageID(), financialTransaction);
                return message;
            }
        });

        logger.info("Transaction sent for validation...");
        while (!validationMessages.containsKey(financialTransaction.getId())) {
            try {
                logger.info("Waiting for validation...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new InvalidStateException("Unexpected thread  interruption occurred...");
            }
        }

        ValidationMessageDTO validationMessage = validationMessages.get(financialTransaction.getId());
        logger.info("Validation received..." + validationMessage.toString());
        validationMessages.remove(financialTransaction.getId());
        financialTransactions.remove(financialTransaction.getId());
        if (validationMessage.getValidationStatus().equals(ValidationStatus.VALID)) {
            return transactionRepository.save(financialTransaction);
        } else throw new TransactionValidationException(validationMessage.getValidationDetails());

    }

    @Override
    public List<FinancialTransaction> loadFinancialTransactionsByPayerName(String payerName) {
        return transactionRepository.findByPayerName(payerName);
    }

    @Override
    public Report generateFinancialTransactionsReportByPayerName(String payerName) {
        List<FinancialTransaction> financialTransactions = transactionRepository.findByPayerName(payerName);
        if (financialTransactions.size() == 0) return null;
        Report report = new Report();
        report.setPayerName(payerName);
        report.setCnp(financialTransactions.get(0).getCnpPayer());
        report.setIban(financialTransactions.get(0).getIbanPayer());
        report.setIbanToIbanTransactions(getTransactionSummaryByType(financialTransactions, TransactionType.IBAN_TO_IBAN, payerName));
        report.setIbanToWalletTransactions(getTransactionSummaryByType(financialTransactions, TransactionType.IBAN_TO_WALLET, payerName));
        report.setWalletToIbanTransactions(getTransactionSummaryByType(financialTransactions, TransactionType.WALLET_TO_IBAN, payerName));
        report.setWalletToWalletTransactions(getTransactionSummaryByType(financialTransactions, TransactionType.WALLET_TO_WALLET, payerName));

        return report;
    }

    @JmsListener(destination = "VALIDATION.QUEUE")
    public void validationProcessor(String response, Message message) throws IOException, JMSException {
        logger.info("Validation received..." + response);
        ValidationMessageDTO validationMessage = mapper.readValue(response, ValidationMessageDTO.class);
        validationMessages.put(message.getJMSCorrelationID(), validationMessage);
        logger.info("Validation processed..." + validationMessage.toString());
    }

    private TransactionSummary getTransactionSummaryByType(List<FinancialTransaction> financialTransactions, TransactionType transactionType, String payerName) {
        List<FinancialTransaction> transactionFilteredByType = financialTransactions.stream()
                .filter(transaction -> transaction.getTransactionType().equals(transactionType) && transaction.getPayerName().equals(payerName)).collect(Collectors.toList());

        TransactionSummary transactionSummary = new TransactionSummary();
        transactionSummary.setTransactionType(transactionType);
        transactionSummary.setTransactionsNumber(transactionFilteredByType.size() + " transactions");
        transactionSummary.setTotalAmount(transactionFilteredByType.stream()
                .map(FinancialTransaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
        transactionSummary.setFinancialTransactions(transactionFilteredByType);
        return transactionSummary;
    }
}
