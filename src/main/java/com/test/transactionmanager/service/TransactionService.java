package com.test.transactionmanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.test.transactionmanager.dto.Report;
import com.test.transactionmanager.model.FinancialTransaction;

import java.util.List;

public interface TransactionService {

    /*
    *  Create financial transaction entity
    * @param transaction the entity to create
    * @return the created entity
    */
    FinancialTransaction createFinancialTransaction(FinancialTransaction financialTransaction) throws JsonProcessingException;

    /*
    * Load all financial transactions to a client
    * @param payerName the name of a client
    * @return the list of transactions associated with a client
     */
    List<FinancialTransaction> loadFinancialTransactionsByPayerName(String payerName);


    /*
     * Generate report with all financial transactions to a client grouped by transaction type
     * @param payerName the name of a client
     * @return the transaction report
     */
    Report generateFinancialTransactionsReportByPayerName(String payerName);

}
