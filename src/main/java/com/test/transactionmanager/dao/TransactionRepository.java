package com.test.transactionmanager.dao;

import com.test.transactionmanager.model.FinancialTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<FinancialTransaction, String> {

    List<FinancialTransaction> findByPayerName(String payerName);
}
