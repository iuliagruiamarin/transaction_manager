package com.test.transactionmanager.dto;

public class Report {

    private String payerName;

    private String cnp;

    private String iban;

    private TransactionSummary ibanToIbanTransactions;

    private TransactionSummary ibanToWalletTransactions;

    private TransactionSummary walletToIbanTransactions;

    private TransactionSummary walletToWalletTransactions;

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public TransactionSummary getIbanToIbanTransactions() {
        return ibanToIbanTransactions;
    }

    public void setIbanToIbanTransactions(TransactionSummary ibanToIbanTransactions) {
        this.ibanToIbanTransactions = ibanToIbanTransactions;
    }

    public TransactionSummary getIbanToWalletTransactions() {
        return ibanToWalletTransactions;
    }

    public void setIbanToWalletTransactions(TransactionSummary ibanToWalletTransactions) {
        this.ibanToWalletTransactions = ibanToWalletTransactions;
    }

    public TransactionSummary getWalletToIbanTransactions() {
        return walletToIbanTransactions;
    }

    public void setWalletToIbanTransactions(TransactionSummary walletToIbanTransactions) {
        this.walletToIbanTransactions = walletToIbanTransactions;
    }

    public TransactionSummary getWalletToWalletTransactions() {
        return walletToWalletTransactions;
    }

    public void setWalletToWalletTransactions(TransactionSummary walletToWalletTransactions) {
        this.walletToWalletTransactions = walletToWalletTransactions;
    }
}
