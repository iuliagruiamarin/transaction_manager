package com.test.transactionmanager.dto;

public enum ValidationStatus {
    VALID,
    INVALID;
}
