package com.test.transactionmanager.dto;

import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.model.enums.TransactionType;

import java.math.BigDecimal;
import java.util.List;

public class TransactionSummary {

    private TransactionType transactionType;

    private String transactionsNumber;

    private BigDecimal totalAmount;

    private List<FinancialTransaction> financialTransactions;

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionsNumber() {
        return transactionsNumber;
    }

    public void setTransactionsNumber(String transactionsNumber) {
        this.transactionsNumber = transactionsNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<FinancialTransaction> getFinancialTransactions() {
        return financialTransactions;
    }

    public void setFinancialTransactions(List<FinancialTransaction> financialTransactions) {
        this.financialTransactions = financialTransactions;
    }
}
