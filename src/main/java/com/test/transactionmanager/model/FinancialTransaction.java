package com.test.transactionmanager.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.test.transactionmanager.model.enums.TransactionType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class FinancialTransaction {
    @Id
    @JsonIgnore
    private String id;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    private String ibanPayer;

    private String ibanPayee;

    private String walletPayer;

    private String walletPayee;

    private String cnpPayer;

    private String cnpPayee;

    private String payerName;

    private String payeeName;

    private String description;

    private BigDecimal amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getIbanPayer() {
        return ibanPayer;
    }

    public void setIbanPayer(String ibanPayer) {
        this.ibanPayer = ibanPayer;
    }

    public String getIbanPayee() {
        return ibanPayee;
    }

    public void setIbanPayee(String ibanPayee) {
        this.ibanPayee = ibanPayee;
    }

    public String getWalletPayer() {
        return walletPayer;
    }

    public void setWalletPayer(String walletPayer) {
        this.walletPayer = walletPayer;
    }

    public String getWalletPayee() {
        return walletPayee;
    }

    public void setWalletPayee(String walletPayee) {
        this.walletPayee = walletPayee;
    }

    public String getCnpPayer() {
        return cnpPayer;
    }

    public void setCnpPayer(String cnpPayer) {
        this.cnpPayer = cnpPayer;
    }

    public String getCnpPayee() {
        return cnpPayee;
    }

    public void setCnpPayee(String cnpPayee) {
        this.cnpPayee = cnpPayee;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
