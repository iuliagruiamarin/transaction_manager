package com.test.transactionmanager.utils;

import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.model.enums.TransactionType;

import java.math.BigDecimal;

public class TestUtils {

    public static FinancialTransaction createFinancialTransaction(String payerName, TransactionType transactionType){
       FinancialTransaction financialTransaction= new FinancialTransaction();
        financialTransaction.setTransactionType(transactionType);
        financialTransaction.setAmount(new BigDecimal(100));
        financialTransaction.setPayerName(payerName);
        financialTransaction.setPayeeName("George");
        financialTransaction.setCnpPayer("RO19RZBR8637876275195736");
        financialTransaction.setCnpPayee("RO19ING8637876275195736");
        financialTransaction.setDescription("Transaction 1");
        financialTransaction.setIbanPayer("1960702070713");
        financialTransaction.setIbanPayee("1921002070713");
        return financialTransaction;
    }
}
