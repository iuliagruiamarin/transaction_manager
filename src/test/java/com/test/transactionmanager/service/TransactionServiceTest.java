package com.test.transactionmanager.service;

import com.test.transactionmanager.dao.TransactionRepository;
import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.model.enums.TransactionType;
import com.test.transactionmanager.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jms.core.JmsTemplate;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    private static final String PAYER_NAME = "Bob";
    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionServiceImpl underTest;

    private static FinancialTransaction financialTransaction;

    @Spy
    private JmsTemplate jmsTemplate;

    @Before
    public void init() {
        financialTransaction = TestUtils.createFinancialTransaction(PAYER_NAME, TransactionType.IBAN_TO_IBAN);
        when(transactionRepository.findByPayerName(PAYER_NAME)).thenReturn(Arrays.asList(financialTransaction));
        when(transactionRepository.save(any(FinancialTransaction.class))).thenAnswer(new Answer<FinancialTransaction>() {
            @Override
            public FinancialTransaction answer(InvocationOnMock invocationOnMock) throws Throwable {
                FinancialTransaction financialTransaction = invocationOnMock.getArgument(0);
                if (financialTransaction.getPayerName() == null) {
                    financialTransaction.setPayerName(PAYER_NAME);
                }
                return financialTransaction;
            }
        });
    }

    @Test
    public void testLoadFinancialTransactionsByPayerName() {
        List<FinancialTransaction> financialTransactions = underTest.loadFinancialTransactionsByPayerName(PAYER_NAME);
        assertTrue("There should be one transaction under the test case", financialTransactions.size() == 1);
        assertEquals("Payer name is incorrect", financialTransactions.get(0).getPayerName(), PAYER_NAME);
        verify(transactionRepository, times(1)).findByPayerName(eq(PAYER_NAME));
    }

}