package com.test.transactionmanager.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.transactionmanager.model.FinancialTransaction;
import com.test.transactionmanager.model.enums.TransactionType;
import com.test.transactionmanager.service.TransactionService;
import com.test.transactionmanager.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    private static final String PAYER_NAME = "Bob";
    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private TransactionController transactionController;

    private static FinancialTransaction financialTransaction;

    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void init() throws JsonProcessingException {
        initMocks(this);

        financialTransaction = TestUtils.createFinancialTransaction(PAYER_NAME, TransactionType.IBAN_TO_IBAN);

        given(transactionService.createFinancialTransaction(any(FinancialTransaction.class))).willReturn(financialTransaction);
        when(transactionService.loadFinancialTransactionsByPayerName(PAYER_NAME)).thenReturn(Arrays.asList(financialTransaction));

        mockMvc = MockMvcBuilders.standaloneSetup(transactionController).build();
    }

    @Test
    public void testRetrieveFinancialTransactionsByPayerName() throws Exception {
        mockMvc.perform(get("/transaction/{payerName}", PAYER_NAME))
                .andExpect(status().isOk())
                .andReturn();
        verify(transactionService, times(1)).loadFinancialTransactionsByPayerName(PAYER_NAME);
        verifyNoMoreInteractions(transactionService);

    }

    @Test
    public void testCreateFinancialTransaction() throws Exception {
        mockMvc.perform(post("/transaction")
                .content(mapper.writeValueAsString(financialTransaction))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        verify(transactionService, times(1)).createFinancialTransaction(refEq(financialTransaction));
        verifyNoMoreInteractions(transactionService);
    }

    @Test
    public void testGenerateFinancialTransactionsReportByPayerName() throws Exception {
        mockMvc.perform(get("/transaction/report/{payerName}", PAYER_NAME))
                .andExpect(status().isOk())
                .andReturn();
        verify(transactionService, times(1)).generateFinancialTransactionsReportByPayerName(PAYER_NAME);
        verifyNoMoreInteractions(transactionService);
    }
}